** Java Script Object Notation {JSON} 
Data representation format
Self Describing and so human readable
Used in API data echange
It is language independent

JSON datatypes:
Strings: "Hello World"
Numbers: 12, 8.5 
Boolean: True False
Arrays: [1, 2, 3] ["HTML", "CSS"]
Objects: {"Key":"value"} {"name":"Priyansh"}
Null: Empty or Null Values

*JASON file should always end with .JSON
Always use Key/Value Pair
Data is separated by commas
Curly braces hold Objects
Square braces holds arrays.

{
    "name":"Priyansh"
    "age":20
    "isProgrammer":True
    "skills":["HTML","CSS","JS"]
    "Location":{
        "city": Noida
        "state":Uttar Pradesh
    }
}